DEVICE := oscar

MODEM_IMAGE := firmware-update/NON-HLOS.bin
SBL1_MBN := firmware-update/sbl1.mbn
APPSBOOT_MBN := firmware-update/emmc_appsboot.mbn

TIMESTAMP := $(shell strings $(MODEM_IMAGE) | sed -n 's/.*"Time_Stamp": "\([^"]*\)"/\1/p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's/[ :-]*//g')

HASH_SBL1 := $(shell openssl dgst -r -sha1 $(SBL1_MBN) | cut -d ' ' -f 1)
HASH_APPSBOOT := $(shell openssl dgst -r -sha1 $(APPSBOOT_MBN) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(SBL1_MBN) $(APPSBOOT_MBN)
ifneq ($(HASH_SBL1), 395356b48665343b34d66808f928ae527d8b1dd5)
	$(error SHA-1 of sbl1.mbn mismatch)
endif
ifneq ($(HASH_APPSBOOT), acc94fa3f5cd2d4cb7d368834cd3a91639132dae)
	$(error SHA-1 of emmc_appsboot.mbn mismatch)
endif

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMAGE)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
